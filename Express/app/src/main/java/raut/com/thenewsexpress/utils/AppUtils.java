package raut.com.thenewsexpress.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import raut.com.thenewsexpress.R;

/**
 * Created by 3543 on 06-03-2017.
 */

public class AppUtils {
    /**
     * Custom toast to show application error messages
     *
     * @param context
     * @param message
     */
    public static void displayAppToast(Context context, String message) {
        Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.mipmap.ic_launcher);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setTextColor(context.getResources().getColor(R.color.color_white));
        text.setPadding(10, 0, 10, 0);
        toast.show();
    }

    /**
     * Checks for network is enabled or not
     *
     * @param context
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
