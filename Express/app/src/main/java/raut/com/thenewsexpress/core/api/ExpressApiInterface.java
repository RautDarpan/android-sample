package raut.com.thenewsexpress.core.api;

import raut.com.thenewsexpress.core.api.model.DBNews;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by 3543 on 06-03-2017.
 */

public interface ExpressApiInterface {

    /**
     * Interface for Fecthing News From API
     * https://newsapi.org/v1/articles?source=the-times-of-india&sortBy=latest&apiKey={API_KEY}
     */
    @GET("/articles?source=the-hindu&sortBy=latest&apiKey=2565fb1656cd4034b4b9650302126e45")
    void getExpressNewsFromServer(Callback<DBNews> dbNewsCallback);
}
