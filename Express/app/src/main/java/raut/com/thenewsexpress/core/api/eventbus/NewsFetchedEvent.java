package raut.com.thenewsexpress.core.api.eventbus;

import java.util.List;

import raut.com.thenewsexpress.core.api.model.DBArticle;

public class NewsFetchedEvent {
    public final List<DBArticle> dbArticleList;
    public NewsFetchedEvent(List<DBArticle> dbArticleList) {
        this.dbArticleList = dbArticleList;
    }
}