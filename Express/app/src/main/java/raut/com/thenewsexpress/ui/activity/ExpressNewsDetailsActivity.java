package raut.com.thenewsexpress.ui.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import raut.com.thenewsexpress.R;
import raut.com.thenewsexpress.core.api.model.DBArticle;

public class ExpressNewsDetailsActivity extends AppCompatActivity {

    @BindView(R.id.ntitle)
    TextView lblTitle;
    @BindView(R.id.ndetails)
    TextView lblDetails;
    @BindView(R.id.arrow_back)
    ImageView ivBack;
    @BindView(R.id.iv_large)
    ImageView ivLarge;
    private DBArticle dbArticle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_express_news_details);
        ButterKnife.bind(this);
        Bundle b = this.getIntent().getExtras();
        if (b != null) {
            dbArticle = (DBArticle) b.getSerializable("news");
            setUI();
        }
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Method to set UI based on Details
     */
    private void setUI() {
        lblTitle.setText(dbArticle.getTitle());
        lblDetails.setText(dbArticle.getDescription());
        String URL = dbArticle.getUrlToImage();
        if (URL != null && !URL.isEmpty()) {
            Glide.with(this)
                    .load(URL)
                    .crossFade()
                    .into(ivLarge);
        }
    }
}
