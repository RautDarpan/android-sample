package raut.com.thenewsexpress.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import raut.com.thenewsexpress.R;
import raut.com.thenewsexpress.core.api.model.DBArticle;
import raut.com.thenewsexpress.ui.activity.ExpressNewsDetailsActivity;

/**
 * Created by 3543 on 03-10-2016.
 */

public class ExpressNewsAdapter extends RecyclerView.Adapter<ExpressNewsAdapter.ExpressNewsViewHolder> {

    private List<DBArticle> newsList;
    Context context;

    public ExpressNewsAdapter(List<DBArticle> newsList, Context context) {
        this.newsList = newsList;
        this.context = context;
    }

    public class ExpressNewsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view)
        CardView cardView;
        @BindView(R.id.title)
        TextView cardNewsTitle;
        @BindView(R.id.count)
        TextView cardNewsDate;
        @BindView(R.id.thumbnail)
        ImageView thumbnail;

        public ExpressNewsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public ExpressNewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_express_news, parent, false);
        return new ExpressNewsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ExpressNewsViewHolder holder, final int position) {
        final DBArticle news = newsList.get(position);
        holder.cardNewsTitle.setText(news.getTitle());
        holder.cardNewsDate.setText(news.getPublishedAt());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, ExpressNewsDetailsActivity.class);
                DBArticle newObject = newsList.get(position);
                Bundle b = new Bundle();
                b.putSerializable("news", newObject);
                i.putExtras(b);
                context.startActivity(i);
            }
        });

        /** Download the image using Glide **/
        String url = news.getUrlToImage();
        Glide.with(context)
                .load(url)
                .crossFade()
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}