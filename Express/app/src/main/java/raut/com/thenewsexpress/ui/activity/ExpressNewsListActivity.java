
package raut.com.thenewsexpress.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import raut.com.thenewsexpress.R;
import raut.com.thenewsexpress.core.api.ExpressApiInterface;
import raut.com.thenewsexpress.core.api.eventbus.NewsFetchedEvent;
import raut.com.thenewsexpress.core.api.model.DBArticle;
import raut.com.thenewsexpress.core.api.model.DBNews;
import raut.com.thenewsexpress.ui.adapter.ExpressNewsAdapter;
import raut.com.thenewsexpress.utils.AppUtils;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ExpressNewsListActivity extends AppCompatActivity {

    @BindView(R.id.lbl_error)
    TextView lblError;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    private Context context;
    private ArrayList<DBArticle> dbArticleList;
    private ExpressNewsAdapter expressNewsAdapter;
    String BASE_URL = "https://newsapi.org/v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_express_news_list);
        ButterKnife.bind(this);
        context = this;
        // Condition to check if Network Internet is Available
        getNewsFromServer();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Recieve Eventbus Event
     * this is done to avoid runtime error, like activity not found
     * this is to ensure that the view is available, as network call is background task
     *
     * @param event
     */
    public void onNewsFetchedEvent(NewsFetchedEvent event) {
    }

    ;

    /**
     * Method to fetch News From Server Using Retrofit
     */
    private void getNewsFromServer() {

        RestAdapter radapter = new RestAdapter.Builder().setEndpoint(BASE_URL).build();
        ExpressApiInterface expressApiInterface = radapter.create(ExpressApiInterface.class);
        expressApiInterface.getExpressNewsFromServer(new Callback<DBNews>() {
            @Override
            public void success(DBNews dbNews, Response response) {
                dbArticleList = new ArrayList<>();
                if (response.getBody() != null) {
                    dbArticleList = new ArrayList<>();
                    for (DBArticle dbArticle : dbNews.getArticles()) {
                        dbArticleList.add(dbArticle);
                    }
                    populateFetchedNews();
                    expressNewsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void failure(RetrofitError t) {
                //Display Toast In case of Error
                AppUtils.displayAppToast(context, t.getMessage());
            }
        });
    }

    /**
     * Method to populate Fresh News from Server
     */
    private void populateFetchedNews() {
        if (dbArticleList != null && dbArticleList.size() > 0) {
            expressNewsAdapter = new ExpressNewsAdapter(dbArticleList, this);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(expressNewsAdapter);
        } else {
            lblError.setVisibility(View.VISIBLE);
        }

    }
}
